<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('customers')->delete();
        $customers = array(
            array('id' => 1, 'email' => 'johndoe@gmail.com', 'nin' => "CMydodgxhduxjxjxk", 'firstname' => "John",'lastname' => "Doe",'phone' => "720000000",'dob' => "2005-12-15"),
            array('id' => 2, 'email' => 'johndoe@gmail.com', 'nin' => "CMydodgxhduxjxjxk", 'firstname' => "John",'lastname' => "Doe",'phone' => "720000000",'dob' => "2005-12-15"),
            array('id' => 3, 'email' => 'johndoe@gmail.com', 'nin' => "CMydodgxhduxjxjxk", 'firstname' => "John",'lastname' => "Doe",'phone' => "720000000",'dob' => "2005-12-15"),
            array('id' => 4, 'email' => 'johndoe@gmail.com', 'nin' => "CMydodgxhduxjxjxk", 'firstname' => "John",'lastname' => "Doe",'phone' => "720000000",'dob' => "2005-12-15"),
            array('id' => 5, 'email' => 'johndoe@gmail.com', 'nin' => "CMydodgxhduxjxjxk", 'firstname' => "John",'lastname' => "Doe",'phone' => "720000000",'dob' => "2005-12-15"),
            array('id' => 6, 'email' => 'johndoe@gmail.com', 'nin' => "CMydodgxhduxjxjxk", 'firstname' => "John",'lastname' => "Doe",'phone' => "720000000",'dob' => "2005-12-15"),
            array('id' => 7, 'email' => 'johndoe@gmail.com', 'nin' => "CMydodgxhduxjxjxk", 'firstname' => "John",'lastname' => "Doe",'phone' => "720000000",'dob' => "2005-12-15"),
            array('id' => 8, 'email' => 'johndoe@gmail.com', 'nin' => "CMydodgxhduxjxjxk", 'firstname' => "John",'lastname' => "Doe",'phone' => "720000000",'dob' => "2005-12-15"),
        );
        DB::table('customers')->insert($customers);
    }
}
