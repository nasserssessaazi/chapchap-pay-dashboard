<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //

        DB::table('transactions')->delete();
        $transactions = array(
            array('id' => 1, 'subscribernumber' => '782187993', 'paymentmethod' => "MTN_MOBILE_MONEY", 'status' => "SUCCESSFUL",'amount' => "1000"),
            array('id' => 2, 'subscribernumber' => '782187993', 'paymentmethod' => "MTN_MOBILE_MONEY", 'status' => "SUCCESSFUL",'amount' => "1000"),
            array('id' => 3, 'subscribernumber' => '782187993', 'paymentmethod' => "MTN_MOBILE_MONEY", 'status' => "SUCCESSFUL",'amount' => "1000"),
            array('id' => 4, 'subscribernumber' => '782187993', 'paymentmethod' => "MTN_MOBILE_MONEY", 'status' => "SUCCESSFUL",'amount' => "1000"),
            array('id' => 5, 'subscribernumber' => '782187993', 'paymentmethod' => "MTN_MOBILE_MONEY", 'status' => "SUCCESSFUL",'amount' => "1000"),
            array('id' => 6, 'subscribernumber' => '782187993', 'paymentmethod' => "MTN_MOBILE_MONEY", 'status' => "SUCCESSFUL",'amount' => "1000"),
        );
        DB::table('transactions')->insert($transactions);
    }
}
